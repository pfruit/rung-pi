#!/usr/bin/env python

import os
from flask import Flask, request, redirect, url_for
from werkzeug import secure_filename
from subprocess import call

NEWFILE = '/var/log/rung-new.log'
UPLOAD_FOLDER = '/var/cache/rung'
ALLOWED_EXTENSIONS = set(['rung', 'rg'])

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

def allowed_file(filename):
	return '.' in filename and filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

@app.route('/', methods=['GET', 'POST'])
def upload_files():
	if request.method == "POST":
		file = request.files['file']
		if file and allowed_file(file.filename):
			filename = secure_filename(file.filename)
			file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))

			f = open(NEWFILE, "w")
			f.write('1')
			f.close()

			return "Rung: Upload: Successful"
		else:
			return "Rung: Upload: Unsuccessful"
	else:
		return '''
		<!DOCTYPE HTML>
		<title>Upload File</title>
		<h1>Upload new File</h1>
		<form action="" method=POST enctype=multipart/form-data>
			<input type=file name=file>
			<input type=submit value=upload>
		</form>
		'''

@app.route('/powerdown', methods=['GET'])
def power_down():
	confirm = request.args['confirm']
	if confirm == '1':
		call(['shutdown', '-h', 'now'])
		return "Rung: Powering Down"
	return "Rung: Powering Down Failed"


@app.route('/reboot', methods=['GET'])
def reboot():
	confirm = request.args['confirm']
	if confirm == '1':
		call(['reboot'])
		return "Rung: Rebooting"
	return "Rung: Rebooting failed"


@app.route('/install', methods=['GET'])
def install_app():
	confirm = request.args['confirm']
	if confirm == '1':
		try:
			f = open("/var/cache/rung/installpath.dat","r")
			installPath = f.read()
			installPath = installPath.strip()
			f.close()

			call(['sudo', 'rung-update', installPath])
		except:
			return "Rung: Rung installation not found. Please use setup first."
		return "Rung: Installation Complete"		
	return "Rung: Installation failed"


@app.route('/update', methods=['GET'])
def update():
	confirm = request.args['confirm']
	if confirm == '1':
		try:
			f = open("/var/cache/rung/installpath.dat","r")
			installPath = f.read()
			installPath = installPath.strip()
			f.close()

			call(['git', '--git-dir='+installPath+'/.git/', '--work-tree='+installPath+'/', 'pull'])
			call(['sudo', 'rung-update', installPath])
		except:
			return "Rung: Rung installation not found. Please use setup first."
		return "Rung: Update On"
	return "Rung: Update Failed"


@app.route('/update_system', methods=['GET'])
def update_system():
	confirm = request.args['confirm']
	if confirm == '1':
		call(['sudo', 'apt-get', 'update'])
		call(['sudo', 'apt-get', '-y',  'upgrade'])
		return "Rung: System updated"
	return "Rung: System update failed"


@app.route('/activate', methods=['GET'])
def activate():
	email = request.args['email']
	call(['sudo', 'rung-activate', email])

	f = open('/var/log/rung-activate.log')
	recv = f.read()
	f.close()
	
	return "Rung: \n"+recv


@app.route('/wifi', methods=['GET'])
def wifi_setup():
	ssid = request.args['ssid']
	psk = request.args['psk']

	ssid_present = False
	not_found = True
	f = open('/etc/wpa_supplicant/wpa_supplicant.conf', 'r')
	lines = f.readlines()
	for i in range(len(lines)):
		parts = lines[i].split("=")
		head = parts[0].strip()
		if head == "ssid":
			if parts[1].strip() == '\"'+ssid+'\"':
				ssid_present = True
				not_found = False

		if ssid_present:
			parts = lines[i].split("=")
			head = parts[0].strip()
			if head == "psk":
				fields = lines[i].split("}")
				tmp = ""
				if len(fields) > 1:
					tmp = '}'+fields[len(fields)-1]
				else:
					tmp = '\n'
				lines[i] = parts[0]+'=\"'+psk+'\"'+tmp    
				ssid_present = False

	f.close()

	f = open("/etc/wpa_supplicant/wpa_supplicant.conf","w")
	for line in lines:
		f.write(line)
	f.close()

	if not_found:
		f = open('/etc/wpa_supplicant/wpa_supplicant.conf','a')
		f.write('\n\nnetwork={\n\tssid=\"'+ssid+'\"\n\tpsk=\"'+psk+'\"\n}\n')
		f.close()

	return "Rung: WiFi details uploaded. Reboot now."


if __name__ == "__main__":
	app.run(host="0.0.0.0", port=6565, debug=True)
