Rung Backend Application

Developers:
Abhijit Mahalunkar (abhijit65@gmail.com)

INSTRUCTIONS FOR USERS:

This Rung backend makes use of tethering facility present in Android phones to create a network over USB cable.

Steps for New Users (Pre-loaded Rung Raspian OS):

1. Download image from our repository.
2. Follow steps on https://www.raspberrypi.org/documentation/installation/installing-images/ to install the image
3. Download the Rung App (Free or Pro) from App Store (Android and IOS).
4. Connect USB cable from Android Phone with the Raspberry Pi.
5. Enable tethering on the phone.
6. Setup WiFi via the App.
7. Incase of Pro App, Activate the device using the Rung App.
8. Start uploading the code.

Steps for Existing users (Raspian OS):

1. Setup interfaces file.
	1. Open the file,
		```
		# sudo /etc/network/interfaces
		```
	2. Append the following lines,
		```
		allow-hotplug usb0
		iface usb0 inet static
		address 192.168.42.65
		netmask 255.255.255.0
		```
2. Download the Rung App (Free or Pro) from App Store (Android and IOS).
3. Connect USB cable from Android Phone with the Raspberry Pi.
4. Enable Tethering on the phone. (Make sure the phone has internet)
5. Setup Rung backend.
	```
	# sudo apt-get install git-core
	# git clone git://git.drogon.net/wiringPi
	# cd wiringPi
	# sudo ./build
	# git clone https://pfruit@bitbucket.org/pfruit/rung-pi.git
	# cd rung-pi
	# sudo ./install
	```
6. Setup WiFi via the App.
7. Incase of Pro App, Activate the device using the Rung App.
8. Start uploading the code.
